package com.application.demo.controllers;

import com.application.demo.entity.Stock;
import com.application.demo.services.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/stocks")
public class StockController {

    private final StockService stockService;

    @Autowired
    public StockController(StockService stockService) {
        this.stockService = stockService;
    }

    @GetMapping
    public List<Stock> getAllStocks() {
            return stockService.getAllStocks();
        }

    @PostMapping
    public void addNewStock(@RequestBody Stock stock) {
        stockService.addNewStock(stock);
    }

    @PutMapping(path = "{stockId}")
    public void updateStock(@PathVariable("stockId") Long stockId, @RequestBody Stock stock){
        stockService.updateStock(stockId, stock);
    }

    @DeleteMapping(path = "{stockId}")
    public void deleteStock(@PathVariable("stockId") Long stockId) {
        stockService.deleteStock(stockId);
    }
}
