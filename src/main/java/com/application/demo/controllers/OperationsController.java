package com.application.demo.controllers;

import com.application.demo.entity.Response;
import com.application.demo.enumerations.TransactionalOperation;
import com.application.demo.services.impl.OperationServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Map;

import static java.time.LocalDateTime.now;
import static java.util.Map.of;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/operations")
@RequiredArgsConstructor
public class OperationsController {
    private final OperationServiceImpl operationService;

    @GetMapping("/list")
    public ResponseEntity<Response> getAllOperations() {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .data(of("stocksOperations", operationService.listAllOperations()))
                        .message("Operations retrieved!")
                        .status(OK)
                        .statusCode(OK.value())
                        .build()
        );
    }

    @GetMapping("/list/buy")
    public ResponseEntity<Response> getBuyOperations() {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .data(of("stockBuyOperations", operationService.listBuyOperations()))
                        .message("Buy Operations retrieved!")
                        .status(OK)
                        .statusCode(OK.value())
                        .build()
        );
    }

    @GetMapping("/list/sell")
    public ResponseEntity<Response> getSellOperations() {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .data(of("stockSellOperations", operationService.listSellOperations()))
                        .message("Operations retrieved!")
                        .status(OK)
                        .statusCode(OK.value())
                        .build()
        );
    }

    @GetMapping("/list/{stockToken}")
    public ResponseEntity<Response> getStockOperations(@PathVariable("stockToken") String stockToken) {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .data(of("stockOperationsByToken", operationService.listStockOperations(stockToken)))
                        .message("Operations with " + stockToken + " retrieved!")
                        .status(OK)
                        .statusCode(OK.value())
                        .build()
        );
    }

    @GetMapping("/list/operation/{id}")
    public ResponseEntity<Response> getStockOperationById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .data(of("stockOperationsById", operationService.getOperation(id)))
                        .message("Operation with id: " + id + " retrieved!")
                        .status(OK)
                        .statusCode(OK.value())
                        .build()
        );
    }

}
