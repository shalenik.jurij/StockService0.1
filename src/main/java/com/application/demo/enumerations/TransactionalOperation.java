package com.application.demo.enumerations;

public enum TransactionalOperation {
    BUY_OPERATION("BUY_OPERATION"),
    SELL_OPERATION("SELL_OPERATION");

    private final String operation;

    TransactionalOperation(String operation) {
        this.operation = operation;
    }

    public String getOperation() {
        return operation;
    }

}
