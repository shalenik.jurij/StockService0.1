package com.application.demo.entity;

import lombok.Getter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Table
public class Stock {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique = true) // Restriction on adding to db token that already exist
    private String tokenName;
    private String stockTitle;
    private Double shares;
    private Double reportedPrice;
    private BigDecimal value;

    public Stock() {
    }

    public Stock(String tokenName,
                 String stockTitle,
                 Double shares,
                 Double reportedPrice,
                 BigDecimal value) {
        this.tokenName = tokenName;
        this.stockTitle = stockTitle;
        this.shares = shares;
        this.reportedPrice = reportedPrice;
        this.value = value;
    }


}
