package com.application.demo.entity;

import com.application.demo.enumerations.TransactionalOperation;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class StockOperation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String stockToken;
    private Double shares;
    private Double stockPrice;
    private TransactionalOperation operation;
    private LocalDate date;

    public StockOperation(String stockToken, Double shares, Double stockPrice, TransactionalOperation operation, LocalDate date) {
        this.stockToken = stockToken;
        this.shares = shares;
        this.stockPrice = stockPrice;
        this.operation = operation;
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        StockOperation that = (StockOperation) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
