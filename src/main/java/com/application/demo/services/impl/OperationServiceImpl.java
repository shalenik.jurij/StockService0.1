package com.application.demo.services.impl;

import com.application.demo.entity.StockOperation;
import com.application.demo.enumerations.TransactionalOperation;
import com.application.demo.repositories.OperationsRepository;
import com.application.demo.services.OperationsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collection;
@RequiredArgsConstructor
@Service
@Slf4j
public class OperationServiceImpl implements OperationsService {

    private final OperationsRepository operationsRepository;

    @Override
    public StockOperation createOperation(StockOperation operation) {
        log.info("Added new operation of " + operation.getOperation() + " with " + operation.getStockToken());
        return operationsRepository.save(operation);
    }

    @Override
    public Collection<StockOperation> listAllOperations() {
        log.info("All operations fetched");
        return operationsRepository.findAll(PageRequest.of(0,100)).toList();
    }

    @Override
    public Collection<StockOperation> listBuyOperations() {
        log.info("All Buy operations fetched");
        return operationsRepository.findAllBuyStockOperations("BUY_OPERATION");
    }

    @Override
    public Collection<StockOperation> listSellOperations() {
        log.info("All Sell operations fetched");
        return operationsRepository.findAllSellStockOperations(TransactionalOperation.SELL_OPERATION.getOperation());
    }

    @Override
    public Collection<StockOperation> listStockOperations(String stockToken) {
        log.info("All operations of " + stockToken + " fetched");
        return operationsRepository.findStockOperationsByStockTitle(stockToken);
    }

    @Override
    public StockOperation getOperation(Long id) {
        log.info("Operation with id: " + id + " fetched");
        return operationsRepository.findById(id).get();
    }

    @Override
    public StockOperation updateOperation(Long id) {

        log.info("Operation with id: " + id + " updated");

        StockOperation byId = operationsRepository.getById(id);


        //TODO REWRITE RETURN STATEMENT
        return operationsRepository.getById(id);
    }

    @Override
    public boolean deleteOperation(Long id) {
        log.info("Operation with id: " + id + " deleted");
        operationsRepository.deleteById(id);
        return Boolean.TRUE;
    }
}
