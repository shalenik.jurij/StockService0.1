package com.application.demo.services;

import com.application.demo.entity.Stock;
import com.application.demo.repositories.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

// TODO Create business logic in controller for validation
@Service
public class StockService {

    private final StockRepository stockRepository;

    @Autowired
    public StockService(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    public List<Stock> getAllStocks() {
        return stockRepository.findAll();
    }

    public void addNewStock(Stock stock) {
        stockRepository.save(stock);
    }

    public void deleteStock(Long stockId) {
        if (!stockRepository.existsById(stockId)) {
            throw new IllegalStateException("The stock with such id: " + stockId + " doesn`t exist");
        }
        stockRepository.deleteById(stockId);
    }

    public void updateStock(Long stockId, Stock stock) {
        if (!stockRepository.existsById(stockId)) {
            throw new IllegalStateException("The stock with such id: " + stockId + " doesn`t exist");
        }
        Optional<Stock> stockRepositoryById = stockRepository.findById(stockId);
        stockRepositoryById = Optional.ofNullable(stock);

    }
}
