package com.application.demo.services;

import com.application.demo.entity.StockOperation;
import com.application.demo.enumerations.TransactionalOperation;

import java.util.Collection;

public interface OperationsService {
    StockOperation createOperation(StockOperation operation);
    Collection<StockOperation> listAllOperations();
    Collection<StockOperation> listBuyOperations();
    Collection<StockOperation> listSellOperations();
    Collection<StockOperation> listStockOperations(String stockToken);

    StockOperation getOperation(Long id);
    StockOperation updateOperation(Long id);
    boolean deleteOperation(Long id);
}
