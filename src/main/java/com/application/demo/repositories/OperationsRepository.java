package com.application.demo.repositories;

import com.application.demo.entity.Stock;
import com.application.demo.entity.StockOperation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OperationsRepository extends JpaRepository<StockOperation, Long> {

    @Query("SELECT s FROM StockOperation s WHERE s.stockToken = ?1")
    Optional<StockOperation> findStockOperationByStockTitle(String title);

    @Query("SELECT s from StockOperation s WHERE s.stockToken = ?1")
    List<StockOperation> findStockOperationsByStockTitle(String stockToken);

    @Query("SELECT s from StockOperation s WHERE s.operation = ?1")
    List<StockOperation> findAllBuyStockOperations(String title);

    @Query("SELECT s from StockOperation s WHERE s.operation = ?1")
    List<StockOperation> findAllSellStockOperations(String title);
}
