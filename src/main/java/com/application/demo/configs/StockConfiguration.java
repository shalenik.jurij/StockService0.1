package com.application.demo.configs;

import com.application.demo.entity.Stock;
import com.application.demo.entity.StockOperation;
import com.application.demo.enumerations.TransactionalOperation;
import com.application.demo.repositories.OperationsRepository;
import com.application.demo.repositories.StockRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Configuration
public class StockConfiguration {
    @Bean
    CommandLineRunner commandLineRunner (StockRepository stockRepository) {
        return args -> {
            Stock tesla = new Stock("TSLA","Tesla",0.3,111.0, BigDecimal.valueOf(111));
            Stock vg = new Stock("SPCE","Virgin Galactic",0.3,111.0, BigDecimal.valueOf(111));

            stockRepository.saveAll(List.of(tesla,vg));
        };
    }

    @Bean
    CommandLineRunner commandLineRunner1 (OperationsRepository operationsRepository) {
        return args -> {
            StockOperation teslaBuy = new StockOperation("TSLA",0.22044769,776.42, TransactionalOperation.BUY_OPERATION, LocalDate.of(2022,03,15));
            StockOperation vgBuy = new StockOperation("SPCE",14.7275405,6.79, TransactionalOperation.BUY_OPERATION, LocalDate.of(2022,03,21));


            operationsRepository.saveAll(List.of(teslaBuy,vgBuy));
        };
    }
}
