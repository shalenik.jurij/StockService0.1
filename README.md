# Stock Service

Stock Service is simple Java Spring Boot application for own custom usage. 
Currently only implemented on backend, but it already connected with Angular and frontend is in stage of creating.

## Start the project locally

### Required to install 

* Java 17
* PostgreSQL

### How to run

1. You should create database `Stocks` and set your db username as `'postgres'` and password as `'root'` .

## Starting stage

Application starting config create 2. Stocks and 2. StockOperations in your account 

```java
Stock tesla = new Stock("TSLA","Tesla",0.3,111.0, BigDecimal.valueOf(111));
Stock vg = new Stock("SPCE","Virgin Galactic",0.3,111.0, BigDecimal.valueOf(111));
```

```java
StockOperation teslaBuy = new StockOperation("TSLA",0.22044769,776.42, TransactionalOperation.BUY_OPERATION, LocalDate.of(2022,03,15));
StockOperation vgBuy = new StockOperation("SPCE",14.7275405,6.79, TransactionalOperation.BUY_OPERATION, LocalDate.of(2022,03,21));
```

You can deal with your stocks via HTTP requests as well as with StockOperations , besides they also can be filtered by operations(Buy, Sell).

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
